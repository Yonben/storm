# Storm Black

## Task Given

Your task is to implement a policy engine.

The policy engine tries to match rules in order, and decides a verdict for a given packet by the first rule that matched it.

A rule consists of:

- Fields to match
- Source IP address / range / subnet
- Destination IP address / range / subnet
- Destination hostname - value or RegEx
- Verdict
  - Block / Approve / Scan

The interface we need you to implement is:

- A rule creation interface
- A rule viewer that shows the cretad rules in order (bonus: re-order them with drag and drop)
- A test interface that gets information for a packet, runs the rules, and shows the first rule that matched it (if any), and it's verdict (if any)

The interface doesn't need to be especially slick or designed, don't put too much effort into it.

You'll be "scored" by code extensibility, structure, simplicity.

## Technical Decisions

### Stack

The stack I used for this task is:

- **React** app created with [Create-React-App](https://github.com/facebook/create-react-app)
- **Typescript** for type-checking, code clarity and reducing runtime errors
- **Wallaby** to help with TDD

### Tradeoffs, "risky" decisions and pain points

Some tradeoffs and technical decisions have been made in order to focus on what seemed important to me:

- No data store and such have been used.
  It limits the scalability of the application if it were to grow a lot.
  However, as we know the app is small and will stay small, it would be overkill and would immensely complicate the code if I added it.
  We also can use the React Context if we wanted to decouple the data (which still seemed unneeded here).

- I make some assumptions about the library and code found on the internet.
  For example the code to transform and check IPs (`ip.ts`) has been found online and my lack of knowledge in the topic forces
  me to assume it works properly.

- 0.0.0.0 subnets don't work. Coming back to the previous point, I doesn't seem to work. I could have made a special case for it
  but decided against that and hope I'll get to learn a lot more about it and be able to handle that case in the future.

- Some places could be DRYer: for example the Creator and Packet forms use repetitive text inputs that work in the same way.
  We can also find that I already made a generic text input but had some type issues.
