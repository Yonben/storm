import { Field, FieldProps, FormikProps } from 'formik';
// @ts-ignore
import { Radio } from 'formik-semantic-ui';
import React from 'react';
import { Form, Input, Label } from 'semantic-ui-react';
import { TesterFormValues } from './TesterForm';

export function TesterInnerForm(props: FormikProps<TesterFormValues>) {
  const { touched, errors, isSubmitting, isValidating, handleSubmit } = props;
  return (
    <Form onSubmit={handleSubmit} size="small">
      <Field
        type="text"
        name="src"
        render={({ field }: FieldProps) => (
          <Form.Field>
            <label>Source IP</label>
            <Input
              fluid={true}
              placeholder="Single IP / Range / Subnet"
              name="src"
              value={field.value}
              error={!!errors.src}
              onChange={field.onChange}
              onBlur={field.onBlur}
            />
            {touched.src && errors.src && (
              <Label basic={true} color="red" pointing={true}>
                {errors.src}
              </Label>
            )}
          </Form.Field>
        )}
      />

      <Field
        type="text"
        name="dst"
        render={({ field }: FieldProps) => (
          <Form.Field>
            <label>Destination IP</label>
            <Input
              fluid={true}
              placeholder="Single IP / Range / Subnet"
              name="dst"
              value={field.value}
              error={!!errors.dst}
              onChange={field.onChange}
              onBlur={field.onBlur}
            />
            {touched.dst && errors.dst && (
              <Label basic={true} color="red" pointing={true}>
                {errors.dst}
              </Label>
            )}
          </Form.Field>
        )}
      />

      <Field
        type="text"
        name="dst_hostname"
        render={({ field }: FieldProps) => (
          <Form.Field>
            <label>Destination Hostname</label>
            <Input
              fluid={true}
              placeholder="Hostname or Regex"
              name="dst_hostname"
              value={field.value}
              error={!!errors.dst_hostname}
              onChange={field.onChange}
              onBlur={field.onBlur}
            />
            {touched.dst_hostname && errors.dst_hostname && (
              <Label basic={true} color="red" pointing={true}>
                {errors.dst_hostname}
              </Label>
            )}
          </Form.Field>
        )}
      />

      <Form.Button type="submit" loading={isValidating} disabled={isSubmitting}>
        Submit
      </Form.Button>
    </Form>
  );
}
