import PacketTester from './tester.lib';
import { Verdict, Rule } from '../../interfaces/rule';
import { Packet } from '../../interfaces/packet';

test('Packet (from 1.2.3.4 | with all matching rules) should be scanned', () => {
  const packet: Packet = {
    dst: '54.22.136.1',
    dst_hostname: 'developers.pornhub.com',
    src: '1.2.3.4',
  };
  const rule: Rule = {
    id: '123',
    dst: '0.0.0.0/1',
    dst_hostname: '.*',
    src: '1.2.3.4',
    verdict: Verdict.SCAN,
  };
  const packetTester = new PacketTester({ packet });
  const verdict = packetTester.judgePacket(rule);
  expect(verdict).toEqual('scan');
});

test('Packet in matching range should be scanned', () => {
  const packet: Packet = {
    dst: '54.22.136.1',
    dst_hostname: 'developers.pornhub.com',
    src: '1.2.3.4',
  };
  const rule: Rule = {
    id: '123',
    dst: '52.125.234.123-55.234.123.654',
    dst_hostname: '.*',
    src: '1.2.3.4',
    verdict: Verdict.SCAN,
  };
  const packetTester = new PacketTester({ packet });
  const verdict = packetTester.judgePacket(rule);
  expect(verdict).toEqual('scan');
});

test('Packet in matching subnet should be scanned', () => {
  const packet: Packet = {
    dst: '54.22.136.1',
    dst_hostname: 'developers.pornhub.com',
    src: '1.2.3.4',
  };
  const rule: Rule = {
    id: '123',
    dst: '54.22.136.0/29',
    dst_hostname: '.*',
    src: '1.2.3.4',
    verdict: Verdict.SCAN,
  };
  const packetTester = new PacketTester({ packet });
  const verdict = packetTester.judgePacket(rule);
  expect(verdict).toEqual('scan');
});

test('Not matching packet should be ignored', () => {
  const packet: Packet = {
    dst: '54.22.136.1',
    dst_hostname: 'developers.pornhub.com',
    src: '1.2.2.4',
  };
  const rule: Rule = {
    id: '123',
    dst: '0.0.0.0/1',
    dst_hostname: '.*',
    src: '1.2.3.4',
    verdict: Verdict.SCAN,
  };
  const packetTester = new PacketTester({ packet });
  const verdict = packetTester.judgePacket(rule);
  expect(verdict).toEqual('none');
});

test('0.0.0.0 CIDR should work', () => {
  const packet: Packet = {
    dst: '0.0.0.2',
    dst_hostname: 'developers.pornhub.com',
    src: '10.0.0.4',
  };
  const rule: Rule = {
    id: '123',
    dst: '0.0.0.0/32',
    dst_hostname: '.*\.pornhub\.com',
    src: '10.0.0.0-10.0.0.255',
    verdict: Verdict.BLOCK,
  };

  const packetTester = new PacketTester({ packet });
  console.log(packetTester.processPacket(rule))
  const verdict = packetTester.judgePacket(rule);
  expect(verdict).toEqual('block');
})
