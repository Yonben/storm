import { Packet, TriggeredFlags } from '../../interfaces/packet';
import { Rule, Verdict } from '../../interfaces/rule';
import { ip4ToInt, isIp4InCidrs } from '../../utils/ip';

enum RULE_IP_TYPE {
  SINGLE = 'single',
  RANGE = 'range',
  SUBNET = 'subnet',
}
class PacketTester {
  packet: Packet;

  constructor({ packet }: { packet: Packet; }) {
    this.packet = packet;
  }


  /**
   * This method, when provided a Rule, returns a list of flags triggered by
   * said rule.
   *
   * @param {Rule} rule
   * @returns {TriggeredFlags}
   * @memberof PacketTester
   */
  public processPacket(rule: Rule): TriggeredFlags {
    const flags: TriggeredFlags = {
      src: false,
      dst: false,
      dst_hostname: false,
    };
    flags.src = this.ipCheck({
      packetIp: this.packet.src,
      ruleIp: rule.src,
    });
    flags.dst = this.ipCheck({
      packetIp: this.packet.dst,
      ruleIp: rule.dst,
    });
    const hostnameRegex = new RegExp(rule.dst_hostname);
    flags.dst_hostname = hostnameRegex.test(this.packet.dst_hostname);

    return flags;
  }


  /**
   * This method is made to help us interpret the results of the processPacket method.
   * It takes the returned flags and checks if it triggered the rule, then returns the Verdict. 
   *
   * @param {Rule} rule
   * @returns {Verdict}
   * @memberof PacketTester
   */
  public judgePacket(rule: Rule): Verdict {
    const flags = this.processPacket(rule);
    if (Object.values(flags).every((flag: boolean) => flag === true)) {
      return rule.verdict;
    }
    return Verdict.NONE;
  }

  private getIpType(ruleIp: string) {
    if (ruleIp.includes('/')) {
      return RULE_IP_TYPE.SUBNET;
    } else if (ruleIp.includes('-')) {
      return RULE_IP_TYPE.RANGE;
    }
    return RULE_IP_TYPE.SINGLE;
  }


  /**
   * This function check the IP against the rule depending on the IP type.
   *
   * @private
   * @param {{ packetIp: string; ruleIp: string }} { packetIp, ruleIp }
   * @returns {boolean}
   * @memberof PacketTester
   */
  private ipCheck({ packetIp, ruleIp }: { packetIp: string; ruleIp: string }): boolean {
    const ipType = this.getIpType(ruleIp);
    switch (ipType) {
      case RULE_IP_TYPE.SINGLE:
        return ruleIp === packetIp;

      case RULE_IP_TYPE.RANGE:
        const [startIp, endIp] = ruleIp.split('-');
        const startIpInt = ip4ToInt(startIp);
        const endIpInt = ip4ToInt(endIp);
        const packetIpInt = ip4ToInt(packetIp);
        return packetIpInt >= startIpInt && packetIpInt <= endIpInt;

      case RULE_IP_TYPE.SUBNET:
        return isIp4InCidrs(packetIp, [ruleIp]);

      default:
        return false;
    }
  }
}

export default PacketTester;
