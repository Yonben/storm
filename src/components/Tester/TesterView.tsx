import * as React from 'react';
import { TesterForm } from './TesterForm';
import { Rule, TriggeredRule } from '../../interfaces/rule';
import { Packet } from '../../interfaces/packet';
import TriggeredRulesList from './TriggeredRulesList';

export interface TesterViewProps {
  testPacket: ((packet: Packet) => void);
  triggeredRules: TriggeredRule[];
}

export default class TesterView extends React.Component<TesterViewProps> {
  public render() {
    return (
      <React.Fragment>
        <TriggeredRulesList triggeredRules={this.props.triggeredRules} />
        <TesterForm testPacket={this.props.testPacket} />
      </React.Fragment>
    );
  }
}
