import * as React from 'react';
import { TriggeredRule, Verdict } from '../../interfaces/rule';
import { List, Message, StrictIconProps } from 'semantic-ui-react';

interface TriggeredRulesListProps {
  triggeredRules: TriggeredRule[];
}

type IconsMap = {
  [key in Verdict]: {
    name: StrictIconProps['name'];
    color: StrictIconProps['color'];
  }
};

const iconsMap: IconsMap = {
  approve: { name: 'checkmark', color: 'green' },
  scan: { name: 'cogs', color: 'yellow' },
  block: { name: 'lock', color: 'red' },
  none: { name: undefined, color: undefined },
};

const TriggeredRulesList: React.SFC<TriggeredRulesListProps> = ({
  triggeredRules,
}) => {
  if (triggeredRules.length === 0) {
    return null;
  }
  if (
    triggeredRules.length === 1 &&
    triggeredRules[0].verdict === Verdict.NONE
  ) {
    return (
      <Message>
        <Message.Header>No rule triggered</Message.Header>
      </Message>
    );
  }
  return (
    <List divided relaxed>
      {triggeredRules.map(triggeredRule => (
        <List.Item key={triggeredRule.id}>
          <List.Icon
            name={iconsMap[triggeredRule.verdict].name}
            size="large"
            verticalAlign="middle"
            color={iconsMap[triggeredRule.verdict].color}
          />
          <List.Content>
            <List.Header as="a">Rule Triggered: {triggeredRule.id}</List.Header>
            <List.Description as="a">
              Verdict: {triggeredRule.verdict}
            </List.Description>
          </List.Content>
        </List.Item>
      ))}
    </List>
  );
};

export default TriggeredRulesList;
