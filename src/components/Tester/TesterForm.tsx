import { FormikErrors, withFormik } from 'formik';
import { Packet } from '../../interfaces/packet';
import { isValidHost, isValidIp } from '../../utils/validators';
import { TesterInnerForm } from './TesterInnerForm';

interface TesterFormProps {
  testPacket: ((packet: Packet) => void);
}

export interface TesterFormValues {
  src: '';
  dst: '';
  dst_hostname: '';
}

// Wrap our form with the withFormik HoC
export const TesterForm = withFormik<TesterFormProps, TesterFormValues>({
  mapPropsToValues: () => {
    return {
      dst: '',
      dst_hostname: '',
      src: '',
    };
  },

  validate: (values: TesterFormValues) => {
    const errors: FormikErrors<TesterFormValues> = {};
    if (!values.src) {
      errors.src = 'Source IP is required.';
    } else if (!isValidIp(values.src)) {
      errors.src =
        'Source IP: Invalid IP format (valid formats: Single IP / Range / Subnet)';
    }

    if (!values.dst) {
      errors.dst = 'Destination IP is required.';
    } else if (!isValidIp(values.dst)) {
      errors.dst =
        'Destination IP: Invalid IP format (valid formats: Single IP / Range / Subnet)';
    }

    if (!values.dst_hostname) {
      errors.dst_hostname = 'Destination Hostname is required.';
    } else if (!isValidHost(values.dst_hostname)) {
      errors.dst_hostname =
        'Destination Hostname values is invalid (make sure you entered a valid regex).';
    }
    return errors;
  },

  handleSubmit: (values, formikBag) => {
    const { src, dst, dst_hostname } = values;
    const packet: Packet = {
      dst,
      dst_hostname,
      src,
    };

    formikBag.props.testPacket(packet);
    formikBag.setSubmitting(false);
  },
})(TesterInnerForm);
