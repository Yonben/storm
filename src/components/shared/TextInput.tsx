import * as React from 'react';
import { Field, FormikProps, FieldProps } from 'formik';
import { Form, Input, Label } from 'semantic-ui-react';
import { CreatorFormValues } from '../Creator/CreatorForm';

interface TextInputProps extends FormikProps<CreatorFormValues> {
  name: string;
  placeholder: string;
  label: string;
}

const TextInput: React.SFC<any> = ({ touched, errors, name, placeholder, label }) => {
  return (
    <Field
      type="text"
      name={name}
      render={({ field }: FieldProps) => (
        <Form.Field>
          <label>{label}</label>
          <Input
            fluid={true}
            placeholder={placeholder}
            name={name}
            value={field.value}
            error={!!errors[name]}
            onChange={field.onChange}
            onBlur={field.onBlur}
          />
          {touched[name] && errors[name] && (
            <Label basic={true} color="red" pointing={true}>
              {errors[name]}
            </Label>
          )}
        </Form.Field>
      )}
    />
  )
};

export default TextInput;