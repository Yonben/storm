import { FormikErrors, withFormik } from 'formik';
import { Rule, Verdict } from '../../interfaces/rule';
import { generatePseudoUniqueID } from '../../utils/generateId';
import { isValidHost, isValidIp } from '../../utils/validators';
import { CreatorInnerForm } from './CreatorInnerForm';

interface CreatorFormProps {
  addRule: ((newRule: Rule) => void);
}

export interface CreatorFormValues {
  src: '';
  dst: '';
  dst_hostname: '';
  verdict: Verdict;
}

// Wrap our form with the withFormik HoC
export const CreatorForm = withFormik<CreatorFormProps, CreatorFormValues>({
  mapPropsToValues: () => {
    return {
      dst: '',
      dst_hostname: '',
      src: '',
      verdict: Verdict.SCAN,
    };
  },

  validate: (values: CreatorFormValues) => {
    const errors: FormikErrors<CreatorFormValues> = {};
    if (!values.src) {
      errors.src = 'Source IP is required.';
    } else if (!isValidIp(values.src)) {
      errors.src =
        'Source IP: Invalid IP format (valid formats: Single IP / Range / Subnet)';
    }

    if (!values.dst) {
      errors.dst = 'Destination IP is required.';
    } else if (!isValidIp(values.dst)) {
      errors.dst =
        'Destination IP: Invalid IP format (valid formats: Single IP / Range / Subnet)';
    }

    if (!values.dst_hostname) {
      errors.dst_hostname = 'Destination Hostname is required.';
    } else if (!isValidHost(values.dst_hostname)) {
      errors.dst_hostname =
        'Destination Hostname values is invalid (make sure you entered a valid regex).';
    }
    return errors;
  },

  handleSubmit: (values, formikBag) => {
    const { src, dst, dst_hostname, verdict } = values;
    const newRule: Rule = {
      dst,
      dst_hostname,
      id: generatePseudoUniqueID(),
      src,
      verdict,
    };

    formikBag.props.addRule(newRule);
    formikBag.resetForm();
  },
})(CreatorInnerForm);
