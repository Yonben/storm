import React from 'react';
import { NavLink, NavLinkProps } from 'react-router-dom';

function NavigationLink(props: NavLinkProps) {
  return (
    <NavLink
      activeStyle={{
        color: 'red',
        fontWeight: 'bold',
      }}
      {...props}
    />
  );
}

export default NavigationLink;
