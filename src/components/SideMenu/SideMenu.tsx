import React from 'react';
import { Menu } from 'semantic-ui-react';
import NavigationLink from './NavigationLink';

function SideMenu(props: {}) {
  return (
    <Menu secondary={true} vertical={true} pointing={true} fluid={true}>
      <Menu.Item>
        <NavigationLink to="/" exact={true}>
          Home
        </NavigationLink>
      </Menu.Item>
      <Menu.Item>
        <NavigationLink to="/list">Rules List</NavigationLink>
      </Menu.Item>
      <Menu.Item>
        <NavigationLink to="/creator">Rule Creator</NavigationLink>
      </Menu.Item>
    </Menu>
  );
}

export default SideMenu;
