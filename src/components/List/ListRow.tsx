import React from 'react';
import { Icon, Table } from 'semantic-ui-react';
import { Rule } from '../../interfaces/rule';

interface ListRowProps {
  rule: Rule;
  moveRuleUp: ((ruleIndex: number) => void);
  moveRuleDown: ((ruleIndex: number) => void);
  index: number;
}

function ListRow(props: ListRowProps) {
  const { rule, moveRuleDown, moveRuleUp, index } = props;
  return (
    <Table.Row
      key={rule.id}
      positive={rule.verdict === 'approve'}
      negative={rule.verdict === 'block'}
    >
      <Table.Cell>
        <Icon
          onClick={() => moveRuleDown(index)}
          fitted={true}
          name="arrow alternate circle down outline"
        />
        <span style={{ padding: '0 5px' }}>{rule.id}</span>
        <Icon
          onClick={() => moveRuleUp(index)}
          fitted={true}
          name="arrow alternate circle up outline"
        />
      </Table.Cell>
      <Table.Cell>{rule.src}</Table.Cell>
      <Table.Cell>{rule.dst}</Table.Cell>
      <Table.Cell>{rule.dst_hostname}</Table.Cell>
      <Table.Cell>{rule.verdict}</Table.Cell>
    </Table.Row>
  );
}

export { ListRow };
