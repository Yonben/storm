import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Container, Divider, Grid, Menu } from 'semantic-ui-react';
import Dashboard from './views/Dashboard';

import SideMenu from './components/SideMenu/SideMenu';

import { Rule } from './interfaces/rule';
import { generatePseudoUniqueID } from './utils/generateId';
import { moveItemDown, moveItemUp } from './utils/moveItemInArray';
import Creator from './views/Creator';
import List from './views/List';

interface AppState {
  rules: Rule[];
}
class App extends Component<{}, AppState> {
  public readonly state = {
    rules: [],
  };

  public async componentDidMount() {
    const rulesData = await import('./data/rules.json');
    const importedRules = rulesData.rules as Rule[];
    const rules: Rule[] = importedRules.map(rule => ({
      ...rule,
      id: generatePseudoUniqueID(),
    }));
    this.setState({ rules });
  }

  public render() {
    return (
      <Router>
        <Container>
          <Menu secondary={true}>
            <Menu.Item header={true}>Storm Black Rules Dashboard</Menu.Item>
          </Menu>
          <Divider />
          <Grid>
            <Grid.Column width={4}>
              <SideMenu />
            </Grid.Column>
            <Grid.Column width={12}>
              <Route path="/" exact={true} component={Dashboard} />
              <Route
                path="/list"
                render={props => (
                  <List
                    rules={this.state.rules}
                    moveRuleUp={this.moveRuleUp}
                    moveRuleDown={this.moveRuleDown}
                    {...props}
                  />
                )}
              />
              <Route
                path="/creator"
                render={props => <Creator addRule={this.addRule} {...props} />}
              />
            </Grid.Column>
          </Grid>
        </Container>
      </Router>
    );
  }
  private addRule = (newRule: Rule) => {
    this.setState((prevState: AppState) => ({
      ...prevState,
      rules: [...prevState.rules, newRule],
    }));
  };

  private moveRuleUp = (ruleIndex: number) => {
    this.setState((prevState: AppState) => ({
      rules: moveItemUp(prevState.rules, ruleIndex),
    }));
  };

  private moveRuleDown = (ruleIndex: number) => {
    this.setState((prevState: AppState) => ({
      rules: moveItemDown(prevState.rules, ruleIndex),
    }));
  };
}

export default App;
