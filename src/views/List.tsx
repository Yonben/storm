import React from 'react';
import { RouteComponentProps } from 'react-router';
import {
  Table,
  TableRowProps,
  Container,
  Divider,
  Header,
} from 'semantic-ui-react';
import { ListRow } from '../components/List/ListRow';
import { Rule, TriggeredRule, Verdict } from '../interfaces/rule';
import { Packet } from '../interfaces/packet';
import PacketTester from '../components/Tester/tester.lib';
import TesterView from '../components/Tester/TesterView';

export interface ListProps extends RouteComponentProps {
  rules: Rule[];
  moveRuleUp: ((ruleIndex: number) => void);
  moveRuleDown: ((ruleIndex: number) => void);
}

interface ListState {
  triggeredRules: TriggeredRule[];
}

export default class List extends React.Component<ListProps, ListState> {
  state = {
    triggeredRules: [],
  };
  public render() {
    return (
      <Container>
        <Table celled={true}>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>#</Table.HeaderCell>
              <Table.HeaderCell>Source IP</Table.HeaderCell>
              <Table.HeaderCell>Destination IP</Table.HeaderCell>
              <Table.HeaderCell>Destination Hostname</Table.HeaderCell>
              <Table.HeaderCell>Verdict</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>{this.generateRows(this.props.rules)}</Table.Body>
        </Table>
        <Divider />
        <Header>Test Custom Packet</Header>
        <TesterView
          testPacket={this.testPacket.bind(this)}
          triggeredRules={this.state.triggeredRules}
        />
      </Container>
    );
  }

  /**
   * This method test the given packet agaisnt the rules and
   * stop at the first triggered rule. It then passes it to the TriggeredRulesList from the view state.
   *
   * @private
   * @param {Packet} packet
   * @memberof List
   */
  private testPacket(packet: Packet) {
    const { rules } = this.props;
    const triggeredRules: TriggeredRule[] = [];
    const packetTester = new PacketTester({ packet });
    for (const rule of rules) {
      const currentVerdict = packetTester.judgePacket(rule);
      if (currentVerdict !== Verdict.NONE) {
        triggeredRules.push({
          id: rule.id,
          verdict: rule.verdict,
        });
        break;
      }
    }
    if (triggeredRules.length === 0) {
      triggeredRules.push({id: '0', verdict: Verdict.NONE});
    }
    this.setState({ triggeredRules });
  }

  private generateRows = (
    rules: Rule[],
  ): Array<React.ReactElement<TableRowProps>> => {
    const rulesRows: Array<React.ReactElement<TableRowProps>> = [];
    if (rules.length === 0) {
      return [<Table.Row disabled={true} key="disabled-row" />];
    }
    rules.forEach((rule, i) => {
      const ruleRow: React.ReactElement<TableRowProps> = (
        <ListRow
          index={i}
          key={rule.id}
          rule={rule}
          moveRuleDown={this.props.moveRuleDown}
          moveRuleUp={this.props.moveRuleUp}
        />
      );
      rulesRows.push(ruleRow);
    });
    return rulesRows;
  };
}
