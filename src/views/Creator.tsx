import React from 'react';
import { RouteComponentProps } from 'react-router';
import { Header } from 'semantic-ui-react';
import { CreatorForm } from '../components/Creator/CreatorForm';
import { Rule } from '../interfaces/rule';
interface CreatorProps extends RouteComponentProps {
  addRule: ((newRule: Rule) => void);
}

class Creator extends React.Component<CreatorProps> {
  public render() {
    return (
      <div>
        <Header>Rule Creator</Header>
        <CreatorForm addRule={this.props.addRule} />
      </div>
    );
  }
}

export default Creator;
