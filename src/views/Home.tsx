import React from 'react';
import { RouteComponentProps } from 'react-router';

export default class Home extends React.Component<RouteComponentProps> {
  public render() {
    return <div>Welcome to Storm Black Technical Test Interface</div>;
  }
}
