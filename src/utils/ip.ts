const ip4ToInt = (ip: string) =>
  ip.split('.').reduce((int, oct) => (int << 8) + parseInt(oct, 10), 0) >>> 0;

const isIp4InCidr = (ip: string) => (cidr: string) => {
  const [range, bits = 32]: any = cidr.split('/');
  const mask = ~(2 ** (32 - bits) - 1);
  return (ip4ToInt(ip) & mask) === (ip4ToInt(range) & mask);
};

const isIp4InCidrs = (ip: string, cidrs: string[]) =>
  cidrs.some(isIp4InCidr(ip));

export { isIp4InCidrs, ip4ToInt };
