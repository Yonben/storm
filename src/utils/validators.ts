const isValidIp = (input: string): boolean => {
  const ipRegex = /^(?:\d{1,3}\.){3}\d{1,3}(?:(?:\-(\d{1,3}\.){3}\d{1,3})|(?:\/\d{1,2}))?$/g;
  return ipRegex.test(input);
};

const isValidHost = (input: string): boolean => {
  try {
    ''.match(new RegExp(input));
    return true;
  } catch (err) {
    return false;
  }
};

export { isValidHost, isValidIp };
