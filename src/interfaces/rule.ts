export enum Verdict {
  APPROVE = 'approve',
  SCAN = 'scan',
  BLOCK = 'block',
  NONE = 'none',
}

export interface Rule {
  id: string;
  src: string;
  dst: string;
  dst_hostname: string;
  verdict: Verdict;
}

export interface TriggeredRule {
  id: string;
  verdict: Verdict;
}
