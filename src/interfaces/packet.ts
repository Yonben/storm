type PacketFields = 'dst' | 'dst_hostname' | 'src';

export type Packet = { [key in PacketFields]: string };
export type TriggeredFlags = { [key in PacketFields]: boolean};
